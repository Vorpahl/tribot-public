package scripts.usa.api.antiban;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.input.Mouse;
import org.tribot.api.interfaces.Clickable07;
import org.tribot.api.interfaces.Positionable;
import org.tribot.api.types.generic.Filter;
import org.tribot.api.util.abc.ABCProperties;
import org.tribot.api.util.abc.ABCUtil;
import org.tribot.api.util.abc.preferences.OpenBankPreference;
import org.tribot.api.util.abc.preferences.TabSwitchPreference;
import org.tribot.api.util.abc.preferences.WalkingPreference;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Projection;
import org.tribot.api2007.types.RSCharacter;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSTile;;

public final class ABC {

	private static ABCUtil abc = null;

	private static int next_run;
	private static int next_eat;
	private static int next_energy_potion;

	private static Boolean should_hover = null;
	private static Boolean should_open_menu = null;
	private static Boolean should_move_to_anticipated = null;

	private static Positionable next_target;
	private static Positionable next_target_closest;

	private static long start_time;
	private static long end_time;
	private static long last_action_time;
	private static long last_combat_time;

	/**
	 * Static initializer
	 */
	static {

		if (abc == null)
			abc = new ABCUtil();

		General.useAntiBanCompliance(true);

	}

	/**
	 * Prevent instantiation of this class.
	 */
	private ABC() {
	}

	/**
	 * Destroys the current instance of ABCUtil and stops all anti-ban threads.
	 * Call this at the end of your script.
	 */
	public static void destroy() {
		abc.close();
		abc = null;
	}

	/**
	 * Gets the instance of ABCUtil
	 * 
	 * @return ABCUtil
	 */
	public static ABCUtil getABC() {
		return abc;
	}

	/**
	 * Checks if we should eat
	 * 
	 * @return true if we should eat
	 */
	public static boolean shouldEat() {

		if (next_eat == 0)
			next_eat = generateNextEat();

		if (Combat.getHPRatio() <= next_eat) {
			next_eat = generateNextEat();
			return true;
		}

		return false;

	}

	/**
	 * Generates the next value to eat at
	 */
	private static int generateNextEat() {

		return ABC.getABC().generateEatAtHP();

	}

	/**
	 * Checks if we should activate run
	 * 
	 * @return true if we activated run
	 */
	public static boolean activateRun() {

		if (next_run == 0)
			next_run = ABC.getABC().generateRunActivation();

		if (!Game.isRunOn() && Game.getRunEnergy() >= next_run) {

			if (Options.setRunOn(true)) {
				next_run = ABC.getABC().generateRunActivation();
				return true;
			}

		}

		return false;
	}

	/**
	 * Generates the next value to drink an energy potion at
	 * 
	 * @return next integer value
	 */
	private static int generateNextEnergyPotion() {

		return General.random(50, 90);

	}

	/**
	 * Checks if we should drink an energy potion
	 * 
	 * @return true if we should drink an energy potion
	 */
	public static boolean shouldDrinkEnergyPotion() {

		if (next_energy_potion == 0)
			next_energy_potion = generateNextEnergyPotion();

		if (Game.getRunEnergy() <= next_energy_potion) {
			next_energy_potion = generateNextEnergyPotion();
			return true;
		}

		return false;

	}

	/**
	 * Performs all timed antiban actions
	 * 
	 * @return true if one action was performed
	 */
	public static boolean performAntiban() {

		return performRotateCamera() || performXPCheck() || performPickupMouse() || performLeaveGame()
				|| performExamineObject() || performRandomRightClick() || performRandomMouseMovement()
				|| performTabsCheck();

	}

	/**
	 * Performs the camera rotation
	 * 
	 * @return true if we rotated the camera
	 */
	public static boolean performRotateCamera() {

		if (ABC.getABC().shouldRotateCamera()) {
			ABC.getABC().rotateCamera();
			return true;
		}

		return false;

	}

	/**
	 * Performs the XP check
	 * 
	 * @return true if we checked the XP
	 */
	public static boolean performXPCheck() {

		if (ABC.getABC().shouldCheckXP()) {
			ABC.getABC().checkXP();
			return true;
		}

		return false;
	}

	/**
	 * Performs the mouse pickup
	 * 
	 * @return true if we picked up the mouse
	 */
	public static boolean performPickupMouse() {

		if (ABC.getABC().shouldPickupMouse()) {
			ABC.getABC().pickupMouse();
			return true;
		}

		return false;

	}

	/**
	 * Performs the leave game
	 * 
	 * @return true if we left the game
	 */
	public static boolean performLeaveGame() {

		if (ABC.getABC().shouldLeaveGame()) {
			ABC.getABC().leaveGame();
			return true;
		}

		return false;

	}

	/**
	 * Performs the examine object
	 * 
	 * @return true if we examined the object
	 */
	public static boolean performExamineObject() {

		if (ABC.getABC().shouldExamineEntity()) {
			ABC.getABC().examineEntity();
			return true;
		}

		return false;

	}

	/**
	 * Performs the random right click
	 * 
	 * @return true if we randomly right click
	 */
	public static boolean performRandomRightClick() {

		if (ABC.getABC().shouldRightClick()) {
			ABC.getABC().rightClick();
			return true;
		}

		return false;

	}

	/**
	 * Performs the random mouse movements
	 * 
	 * @return true if we randomly move the mouse
	 */
	public static boolean performRandomMouseMovement() {

		if (ABC.getABC().shouldMoveMouse()) {
			ABC.getABC().moveMouse();
			return true;
		}

		return false;

	}

	/**
	 * Performs the tabs check
	 * 
	 * @return true if we checked the tabs
	 */
	public static boolean performTabsCheck() {

		if (ABC.getABC().shouldCheckTabs()) {
			ABC.getABC().checkTabs();
			return true;
		}

		return false;

	}

	/**
	 * Checks if we should hover
	 * 
	 * @return true if we should hover
	 */
	public static boolean shouldHover() {

		if (!Mouse.isInBounds())
			return false;

		if (should_hover == null)
			should_hover = generateNextHover();

		return should_hover;

	}

	/**
	 * Generates new hover
	 */
	public static void resetShouldHover() {

		should_hover = null;

	}

	/**
	 * Generates the next value for hovering over an entity
	 * 
	 * @return true if we should hover
	 */
	public static boolean generateNextHover() {

		should_hover = ABC.getABC().shouldHover();

		return should_hover;

	}

	/**
	 * Checks if we should open the menu
	 * 
	 * @return true if we should open the menu
	 */
	public static boolean shouldOpenMenu() {

		if (should_hover == null)
			should_hover = generateNextHover();

		if (should_open_menu == null)
			should_open_menu = generateNextOpenMenu();

		return should_hover && should_open_menu;

	}

	/**
	 * Generates new open menu
	 */
	public static void resetShouldOpenMenu() {

		should_open_menu = null;

	}

	/**
	 * Generates the next value for opening a menu
	 * 
	 * @return true if we should open the menu
	 */
	public static Boolean generateNextOpenMenu() {

		should_open_menu = ABC.getABC().shouldOpenMenu();

		return should_open_menu;

	}

	/**
	 * Checks if we should move to anticipated
	 * 
	 * @return true if we should move to anticipated
	 */
	public static boolean shouldMoveToAnticipated() {

		if (should_move_to_anticipated == null)
			should_move_to_anticipated = generateMoveToAnticipated();

		return should_move_to_anticipated;

	}

	/**
	 * Generates new move to anticipated
	 */
	public static void resetMoveToAnticipated() {

		should_move_to_anticipated = null;

	}

	/**
	 * Generates the next value for move to anticipated
	 * 
	 * @return true if we should move to anticipated
	 */
	public static Boolean generateMoveToAnticipated() {

		should_move_to_anticipated = ABC.getABC().shouldMoveToAnticipated();

		return should_move_to_anticipated;

	}

	/**
	 * Gets your waiting time based on the time you started a task to when that
	 * task ended. If that value is above 5 minutes the data is erroneous and we
	 * should defer to the last time we performed an action, otherwise return 0.
	 * 
	 * @return int waiting time
	 */
	public static int getWaitingTime() {

		long waiting_time = 0;

		if (start_time > 0 && end_time > start_time)
			waiting_time = end_time - start_time;

		if (waiting_time > 0 && waiting_time < 300000)
			return (int) waiting_time;

		long current_time = Timing.currentTimeMillis();

		if (last_action_time > 0 && current_time >= last_action_time)
			waiting_time = current_time - last_action_time;

		if (waiting_time > 0 && waiting_time < 300000)
			return (int) waiting_time;

		return 0;

	}

	/**
	 * Caches the last time an action was performed
	 * 
	 * @param long
	 *            time
	 */
	public static void setLastActionTime(long time) {
		last_action_time = time;
	}

	/**
	 * Caches the last time in combat
	 * 
	 * @param long
	 *            time
	 */
	public static void setCombatTime(long time) {
		last_combat_time = time;
	}

	/**
	 * Checks if you were recently in combat
	 * 
	 * @return true if combat was in the last 10 minutes
	 */
	public static boolean wasCombatRecent() {

		if (last_combat_time == 0)
			return false;

		return (System.currentTimeMillis() - last_combat_time) < 600000;

	}

	/**
	 * Sets your task's start time
	 * 
	 * @return long time
	 */
	public static void setStartTime(long time) {
		start_time = time;
	}

	/**
	 * Gets your start time
	 * 
	 * @return long time
	 */
	public static long getStartTime() {
		return start_time;
	}

	/**
	 * Sets your task's start time
	 * 
	 * @param time
	 */
	public static void setEndTime(long time) {
		end_time = time;
	}

	/**
	 * Gets your end time
	 * 
	 * @return long time
	 */
	public static long getEndTime() {
		return end_time;
	}

	/**
	 * Resets both start and end timers
	 */
	public static void resetTimers() {

		start_time = 0;
		end_time = 0;
		last_action_time = 0;

	}

	public static void sleep(int time) {

		try {
			ABC.getABC().sleep(time);
		} catch (InterruptedException e) {
		}

	}

	/**
	 * Sleeps for the reaction time created by ABC properties
	 * 
	 * @throws InterruptedException
	 */
	public static void sleepReactionTime(int waiting_time, boolean combat, boolean fixed_waiting)
			throws InterruptedException {

		try {

			final ABCProperties props = ABC.getABC().getProperties();

			props.setWaitingTime(waiting_time);
			props.setHovering(ABC.shouldHover());
			props.setMenuOpen(ABC.shouldOpenMenu());
			props.setUnderAttack(combat);
			props.setWaitingFixed(fixed_waiting);

			int reaction_time = ABC.getABC().generateReactionTime();

			System.out.println("Sleeping Reaction Time: " + reaction_time + " ms | Waiting time: " + waiting_time
					+ " | Hovering: " + ABC.shouldHover() + " | Open Menu: " + ABC.shouldOpenMenu() + " | Combat: "
					+ ABC.wasCombatRecent() + " | Fixed Waiting: " + fixed_waiting + " |");

			ABC.sleep(reaction_time);

		} finally {

			resetTimers();

		}

	}

	/**
	 * Generates trackers from ABC properties
	 */
	public static void generateTrackers(int est_waiting, boolean hovering, boolean menu_open, boolean combat,
			boolean fixed_waiting) {

		final ABCProperties props = ABC.getABC().getProperties();

		props.setWaitingTime(est_waiting);
		props.setHovering(hovering);
		props.setMenuOpen(menu_open);
		props.setUnderAttack(combat);
		props.setWaitingFixed(fixed_waiting);

		ABC.getABC().generateTrackers();

	}

	/**
	 * Sleeps for the reaction time created by bit flags
	 * 
	 * @throws InterruptedException
	 */
	public static void sleepReactionTime(int waiting_time, long... options) throws InterruptedException {

		try {

			int reaction_time = ABC.getABC().generateReactionTime(ABC.getABC().generateBitFlags(waiting_time, options));

			System.out
					.println("Sleeping Reaction Time: " + reaction_time + " ms | Waiting time: " + waiting_time + " |");

			ABC.sleep(reaction_time);

		} finally {

			resetTimers();

		}

	}

	/**
	 * Generates trackers from bit flags
	 */
	public static void generateTrackers(int est_waiting, long... options) {

		ABC.getABC().generateTrackers(ABC.getABC().generateBitFlags(est_waiting, options));

	}

	/**
	 * Determines if our current next target is still valid.
	 * 
	 * @param possible_targets
	 *            The possible next targets. This is used just in-case a new
	 *            closest target is available. If that is the case, then we can
	 *            say he next target is invalid and let the script determine a
	 *            new next target.
	 * 
	 * @return boolean
	 */
	public static boolean isNextTargetValid(final Positionable[] possible_targets) {

		if (next_target == null)
			return false;

		final RSTile pos = next_target.getPosition();

		if (pos == null)
			return false;

		if (!Projection.isInViewport(Projection.tileToScreen(pos, 0)))
			return false;

		if (next_target instanceof Clickable07 && !((Clickable07) next_target).isClickable())
			return false;

		if (next_target instanceof RSNPC && !((RSNPC) next_target).isValid())
			return false;

		if (next_target instanceof RSCharacter) {
			final String name = ((RSCharacter) next_target).getName();
			if (name == null || name.trim().equalsIgnoreCase("null"))
				return false;
		}

		if (next_target instanceof RSObject) {
			if (!Objects.isAt(next_target, new Filter<RSObject>() {

				@Override
				public boolean accept(final RSObject o) {
					return o.obj.equals(((RSObject) next_target).obj);
				}

			}))
				return false;
		}

		if (possible_targets != null && possible_targets.length > 0 && next_target_closest != null) {

			final RSTile new_closest_tile = possible_targets[0].getPosition();
			final RSTile orig_closest_tile = next_target_closest.getPosition();
			final RSTile player_pos = Player.getPosition();

			if (new_closest_tile != null && orig_closest_tile != null && player_pos != null) {

				final double new_closest_dist = new_closest_tile.distanceToDouble(player_pos);
				final double orig_closest_dist = orig_closest_tile.distanceToDouble(player_pos);

				if (new_closest_dist < orig_closest_dist)
					return false;

			}

		}

		return true;

	}

	/**
	 * Nullifies the next target.
	 */
	public static void resetNextTarget() {

		next_target = null;
		next_target_closest = null;

	}

	/**
	 * Gets the next target.
	 * 
	 * @return {@link Positionable}, or null if we do not currently have a next
	 *         target.
	 */
	public static Positionable getNextTarget() {

		if (next_target == null || !isNextTargetValid(null))
			return null;

		return next_target;

	}

	/**
	 * Selects the next target, using persistence.
	 * 
	 * @return {@link Positionable}
	 */
	public static Positionable selectNextTarget(final Positionable[] possible_targets) {

		try {

			if (next_target != null && isNextTargetValid(possible_targets))
				return next_target;

			return next_target = selectNextTarget(possible_targets);

		} finally {

			if (next_target != null && possible_targets.length > 0)
				next_target_closest = possible_targets[0];

		}

	}

	/**
	 * Checks if you should switch resources
	 * 
	 * @param competition_count
	 * @return true if you should switch
	 */
	public static boolean shouldSwitchResources(int competition_count) {

		return ABC.getABC().shouldSwitchResources(competition_count);

	}

	/**
	 * Gets your Walking Preference
	 * 
	 * @param int
	 *            distance
	 * @return WalkinPreference
	 */
	public static WalkingPreference getWalkingPreference(int distance) {

		return ABC.getABC().generateWalkingPreference(distance);

	}

	/**
	 * Gets your Tab Switching Preference
	 * 
	 * @return TabSwitchPreference
	 */
	public static TabSwitchPreference getTabSwitchPreference() {

		return ABC.getABC().generateTabSwitchPreference();

	}

	/**
	 * Gets your Bank Opening Preference
	 * 
	 * @return OpenBankPreference
	 */
	public static OpenBankPreference getOpenBankPreference() {

		return ABC.getABC().generateOpenBankPreference();

	}

}