package scripts.usa.api.GrandExchange;

/**
 * TYPE represents the three possible states of a Grand Exchange window.
 */
public enum TYPE {

	BUY, SELL, EMPTY;

}