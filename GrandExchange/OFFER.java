package scripts.usa.api.GrandExchange;

/**
 * OFFER represents the options for creating a new offer.
 */
public enum OFFER {

	BUY, SELL;

}