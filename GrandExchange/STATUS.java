package scripts.usa.api.GrandExchange;

/**
 * STATUS represents the three possible statuses of a Grand Exchange offer.
 */
public enum STATUS {

	EMPTY, IN_PROGRESS, ABORTED, COMPLETE, AVAILABLE_FOR_COLLECTION, DISABLED;

}