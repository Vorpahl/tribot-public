package scripts.usa.api.GeneralStore;

/**
 * Menu to be selected to perform actions on
 */
public enum MENU {

	GENERAL_STORE, INVENTORY;

}