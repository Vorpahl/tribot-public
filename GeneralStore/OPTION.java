package scripts.usa.api.GeneralStore;

/**
 * Option to be performed on the item in the General Store
 */
public enum OPTION {

	BUY_1("Buy 1"), BUY_5("Buy 5"), BUY_10("Buy 10"), SELL_1("Sell 1"), SELL_5("Sell 5"), SELL_10("Sell 10"), SELL_50(
			"Sell 50"), EXAMINE("Examine"), VALUE("Value"), CANCEL("Cancel");

	private final String text;

	OPTION(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

}